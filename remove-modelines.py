#!/usr/bin/env python3

import sys

for filename in sys.argv[1:]:
    data = list(open(filename, 'r').readlines())
    if 'Mode: ' in data[0]:
        line = data[0].rstrip()
        if line.endswith('*/'):
            del data[0]
        else:
            data[0] = '/*\n'
            del data[1]
        open(filename, 'w').write(''.join(data))
