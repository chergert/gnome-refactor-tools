#!/usr/bin/env python3

import sys
import time
import os
import re

LINE1 = 'Christian Hergert <chergert@redhat.com>'
LINE2 = 'Christian Hergert <christian@hergert.me>'
YEAR = time.strftime('%Y')

r1 = re.compile('([0-9]{4})')

for f in sys.argv[1:]:
    data = open(f).read().split('\n')
    pos = 0

    for line in data:
        if LINE1 in line or LINE2 in line:
            if YEAR in line:
                break

            parts = r1.split(line)

            if len(parts) < 2:
                continue

            if '-' in parts:
                parts[parts.index('-')+1] = YEAR
            else:
                parts.insert(2, '-')
                parts.insert(3, YEAR)

            line = ''.join(parts)

            data[pos] = line
            open(f,'w').write('\n'.join(data))
            break

        pos += 1

