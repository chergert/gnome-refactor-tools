#!/usr/bin/env python3

import sys
import os

PREFIX = sys.argv[1]
HEADER = sys.argv[2]

TEMPLATE = '''
#if !defined (%s_INSIDE) && !defined (%s_COMPILATION)
# error "Only <%s> can be included directly."
#endif''' % (PREFIX, PREFIX, HEADER)

MATCH = '%s_INSIDE' % PREFIX

for f in sys.argv[3:]:
    data = open(f).read().split('\n')

    found = False
    for line in data:
        if MATCH in line:
            found = True
            break
    if found:
        print(f, 'has it already, skipping')
        continue

    print("Adding guard to", f)

    pos = data.index('#pragma once')
    data.insert(pos + 1, TEMPLATE)

    open(f,'w').write('\n'.join(data))
