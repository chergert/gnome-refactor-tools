#!/usr/bin/env python3

import sys
import os

LINES = ('IDE_AVAILABLE_IN_ALL',
         'IDE_AVAILABLE_IN_3_28',
         'IDE_AVAILABLE_IN_3_30',)

for f in sys.argv[1:]:
    data = open(f).read().split('\n')
    pos = 0

    for line in data:
        if line in LINES:
            data[pos] = 'IDE_AVAILABLE_IN_3_32'
        pos += 1

    open(f,'w').write('\n'.join(data))
