#!/usr/bin/env python3

import os
import sys

KNOWN_TYPES = ('.c', '.h', '.js', '.py', '.py3', '.cc', '.cpp', '.hh',
               '.cs', '.java', '.hs', '.rs', '.vala')

all_files = []
for basedir in sys.argv[1:]:
    if os.path.isdir(basedir):
        for (base, dirs, files) in os.walk(basedir):
            for file in files:
                if '.' not in file:
                    print("Ignoring", file)
                    continue
                if file[file.rindex('.'):] not in KNOWN_TYPES:
                    print("Ignoring unknown type", file)
                    continue
                all_files.append(os.path.join(base, file))
    else:
        all_files.append(basedir)

for file in all_files:
    with open(file, 'r') as stream:
        lines = []
        found = False
        for line in stream.readlines():
            if 'Copyright (C)' in line:
                line = line.replace('Copyright (C)', 'Copyright')
                found = True
            elif 'Copyright (c)' in line:
                line = line.replace('Copyright (c)', 'Copyright')
                found = True
            elif 'Copyright ©' in line:
                line = line.replace('Copyright ©', 'Copyright')
                found = True
            lines.append(line)
        if found:
            print("Scrubbing (C) in", file)
            with open(file + '.tmp', 'a') as outStream:
                outStream.write(''.join(lines))
            os.rename(file + '.tmp', file)
