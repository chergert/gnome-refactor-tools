#!/usr/bin/env python3

import sys
import os


for f in sys.argv[1:]:
    data = open(f).read().split('\n')
    log_domain_line = -1
    config_line = -1
    lineno = -1

    for line in data:
        lineno += 1

        if line.startswith('#define G_LOG_DOMAIN '):
            log_domain_line = lineno

        if line == '#include "config.h"':
            config_line = lineno

        if config_line > -1 and log_domain_line > -1 and config_line < log_domain_line:
            a = data[log_domain_line]
            b = data[config_line]

            data[log_domain_line] = b
            data[config_line] = a

            open(f,'w').write('\n'.join(data))
            break
