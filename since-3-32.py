#!/usr/bin/env python3

import sys

def startsGtkDoc(line):
    line = line.strip()
    return line.startswith('/**') and not line.endswith('*/')

def getSections(data):
    lines = data.split('\n')
    section = []
    inGtkDoc = False
    for line in lines:
        if startsGtkDoc(line):
            yield '\n'.join(section)
            section = []
            inGtkDoc = True
        section.append(line)
        if inGtkDoc and line.rstrip().endswith('*/'):
            yield '\n'.join(section)
            inGtkDoc = False
            section = []
    if len(section):
        yield '\n'.join(section)

for filename in sys.argv[1:]:
    data = open(filename).read()
    sections = []
    for section in getSections(data):
        lines = section.split('\n')
        if startsGtkDoc(lines[0]):
            offset = lines[0].index('/')
            if 'Since:' in section:
                for i in range(len(lines)):
                    if 'Since:' in lines[i]:
                        lines[i] = (' ' * offset) + ' * Since: 3.32'
                        break
            else:
                lines.insert(-1, (' ' * offset) + ' *')
                lines.insert(-1, (' ' * offset) + ' * Since: 3.32')
            section = '\n'.join(lines)
        sections.append(section)

    open(filename,'w').write('\n'.join(sections))
