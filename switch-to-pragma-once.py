#!/usr/bin/env python3

"""
find . | grep \\.h$ | xargs ./switch-to-pragma-once.py
"""

import sys
import os

for filename in sys.argv[1:]:
    lines = []
    with open(filename, 'r') as stream:
        take_next = False
        word = None
        for line in stream.readlines():
            if line.startswith('#ifndef ') and line.rstrip().endswith('_H'):
                take_next = True
                word = line.strip().split(' ')[1]
                lines.append('#pragma once\n')
                continue
            if take_next and line.strip().startswith('#define ') and word in line:
                take_next = False
                continue
            if line.startswith('#endif') and word and word in line:
                continue
            lines.append(line)
        while lines[-1].strip() == '':
            lines.pop()
    contents = ''.join(lines)
    open(filename + '.tmp', 'w').write(contents)
    os.rename(filename+'.tmp', filename)
