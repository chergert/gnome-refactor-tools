#!/usr/bin/env python3

import sys
import os

LINE = ' * SPDX-License-Identifier: GPL-3.0-or-later'

for f in sys.argv[1:]:
    data = open(f).read().split('\n')
    if LINE in data:
        print(f, 'has it already, skipping')
        continue

    print('Adding GPL-3.0-or-later to', f)

    pos = data.index(' */')
    data.insert(pos, LINE)
    data.insert(pos, ' *')
    open(f,'w').write('\n'.join(data))
