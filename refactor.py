#!/usr/bin/env python3

import os
import sys

#
# GtkWindow -> gtk_window
# GtkHBox -> gtk_hbox
# GtkUIManager -> gtk_ui_manager
# GWeatherLocation -> gweather_location (split_first_cap == FALSE)
# GThemedIcon -> g_themed_icon (slit_first_cap == TRUE)
#

def type_name_mangle(name, split_first_cap=False):
    symbol_name = ""

    for i in range(len(name)):
        if ((name[i] == name[i].upper() and
             ((i > 0 and name[i-1] != name[i-1].upper()) or
              (i == 1 and name[0] == name[0].upper and split_first_cap))) or
            (i > 2 and name[i]  == name[i].upper() and
             name[i-1] == name[i-1].upper() and
             name[i-2] == name[i-2].upper())):
            symbol_name += '_'
        symbol_name += name[i].lower()

    return symbol_name

assert(type_name_mangle("GtkWindow") == 'gtk_window')
assert(type_name_mangle("GtkHBox") == 'gtk_hbox')
assert(type_name_mangle("GtkUIManager") == 'gtk_ui_manager')
assert(type_name_mangle("GWeatherLocation") == 'gweather_location')

if __name__ == '__main__':

    if len(sys.argv) < 3:
        print('usage: cat SRC | %s FromClass ToClass > DST' % sys.argv[0])
        sys.exit(1)

    fromName = sys.argv[1]
    toName = sys.argv[2]

    from_name = type_name_mangle(fromName)
    to_name = type_name_mangle(toName)

    fromNs = fromName[:len(type_name_mangle(fromName).split('_')[0])]
    toNs = toName[:len(type_name_mangle(toName).split('_')[0])]

    fromNS = fromNs.upper()
    toNS = toNs.upper()

    fromCLASS = type_name_mangle(fromName[len(fromNS):]).upper()
    toCLASS = type_name_mangle(toName[len(toNS):]).upper()

    toRename = {
        # "from-thing" => "to-thing"
        '%s' % from_name.replace('_', '-'): '%s' % to_name.replace('_', '-'),

        # "FROM," => "TO,"
        '%s,' % fromNS: '%s,' % toNS,

        # "from_thing" => "to_thing"
        from_name: to_name,

        # "FromThing" => "ToThing"
        fromName: toName,

        # "FROM_IS_THING" => "TO_IS_THING"
        '%s_IS_%s' % (fromNS, fromCLASS): '%s_IS_%s' % (toNS, toCLASS),

        # "FROM_TYPE_THING" => "TO_TYPE_THING"
        '%s_TYPE_%s' % (fromNS, fromCLASS): '%s_TYPE_%s' % (toNS, toCLASS),

        # "FROM_AVAILABLE_IN_ALL" => "TO_AVAILABLE_IN_ALL"
        '%s_AVAILABLE_IN' % fromNS: '%s_AVAILABLE_IN' % toNS,

        # "FROM_INSIDE" => "TO_INSIDE"
        '%s_INSIDE' % fromNS: '%s_INSIDE' % toNS,

        # "FROM_THING" => "TO_THING"
        '%s_%s' % (fromNS, fromCLASS): '%s_%s' % (toNS, toCLASS),
    }

    data = sys.stdin.read()

    for k,v in toRename.items():
        data = data.replace(k,v)

    # try to adjust spacing
    if len(fromNS) != len(toNS):
        lines = data.split('\n')

        lenDiff = len(toNs) - len(fromNs)
        inParams = False
        paramPos = -1

        for i in range(len(lines)):
            line = lines[i]

            if not inParams:
                if len(line) > 0 and (line[0].isalnum() or line[0] == '_'):
                    # Only works for "MyFoo" as first parameter
                    find = '(' + toName
                    if line.endswith(',') and find in line:
                        pos = line.rindex(find)
                        if ',' not in line[pos:-1]:
                            inParams = True
                            paramPos = pos+1
            else:
                newLine = (paramPos * ' ') + line.lstrip()

                # find the last word and add spacing
                splitLine = newLine.split()
                if len(splitLine):
                    lastWordOffset = len(newLine) - len(newLine.split()[-1])

                    if lenDiff > 0:
                        newLine = newLine[:lastWordOffset] + (' '*lenDiff) + newLine[lastWordOffset:]

                    if (splitLine[-1].endswith(')') or
                        splitLine[-1].endswith(';') or
                        'G_GNUC' in splitLine[-1] or
                        '__attribute__' in splitLine[1]):
                        inParams = False
                        paramPos = -1

                lines[i] = newLine


        data = '\n'.join(lines)

    print(data)

