#!/usr/bin/env python3

"""
This locates all the headers found starting from argv[1], which should
be the base include dir. Then it finds all the #include in .c and .h
files and adjusts them to use the proper full path starting from the
include dir. It will only mutate #include "foo.h" style ("" not <>)
includes.

# See what will change
./fixup-includes.py src/ --dry-run

# Perform changes
./fixup-includes.py src/

"""

import os
import re
import sys

_DRY_RUN = False

if '--dry-run' in sys.argv:
    _DRY_RUN = True
    sys.argv.remove('--dry-run')

includedir = sys.argv[1]

all_files = {}

for (base, dirs, files) in os.walk(includedir):
    if base == includedir:
        base = None
    else:
        base = base[len(includedir):]
        while base[0] == '/':
            base = base[1:]
    for file in files:
        if file[-2:] not in ('.c', '.h'):
            continue
        if base is None:
            full = file
        else:
            full = os.path.join(base, file)
        all_files[file] = full


incRegex = re.compile('#\s*include "(.*\.h)"')

for (short, full) in all_files.items():
    lines = []
    with open(os.path.join(includedir, full), 'r') as inStream:
        for line in inStream.readlines():
            match = incRegex.match(line)
            if match:
                included = match.groups()[0]
                name = os.path.basename(included)
                if name not in all_files:
                    # possibly a generated file from .in
                    lines.append(line)
                    continue
                if included != all_files[name]:
                    print("%s: %s => %s" % (full, included, all_files[name]))
                    line = line[0:line.index('"')+1] + all_files[name] + line[line.rindex('"'):]
                    lines.append(line)
                    continue
            lines.append(line)

    if not _DRY_RUN:
        with open(os.path.join(includedir, full + '.tmp'), 'w') as outStream:
            outStream.truncate(0)
            for line in lines:
                outStream.write(line)
        os.rename(os.path.join(includedir, full + '.tmp'), os.path.join(includedir, full))
